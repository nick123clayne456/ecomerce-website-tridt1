from .models import Cart, CartItem

from .views import _cart_id


def counter(request):
    cart_count=0
    if 'admin' in request.path:
        print('\n\n request.path\n: ', request.path, '\n\n')
        return {}
    else:
        try:
            print("\n--------------------------Call '_cart_id()' in 'carts.context_processor.counter'.--------------------------")
            
            cart = Cart.objects.filter(cart_id = _cart_id(request))  

            if request.user.is_authenticated:
                cart_items = CartItem.objects.filter(user=request.user)
            else:
                cart_items = CartItem.objects.filter(cart = cart[0])
            
            for cart_item in cart_items:
                cart_count += cart_item.quantity
        except:
            cart_count = 0
    return dict(cart_count = cart_count)
        





