from django.shortcuts import render, redirect, get_object_or_404

from store.models import Product, Variation, Coupon
from .models import Cart, CartItem

from django.http import HttpResponse

from django.core.exceptions import ObjectDoesNotExist

from django.contrib.auth.decorators import login_required

from django.contrib import messages

# Create your views here.
from datetime import datetime
def _cart_id(request):
    cart = request.session.session_key
    print(f"--------------------------At {datetime.now()}")
    print('\nrequest: \n', request, '\n')
    print('request.session: \n', request.session, '\n')
    print('request.session.session_key: \n', request.session.session_key, '\n')

    if not cart:
        cart = request.session.create()
        print('\n\nNot cart \nrequest.session. ', request.session)
    print('----------------------------------------------------Done----------------------------------------------------\n\n\n')
    return cart



def add_cart_item(request, product_id):
    current_user = request.user

    product = Product.objects.get(id=product_id)

    # If the user is authenticated
    if current_user.is_authenticated:
        product_variation = []
        print("\n\n\n\nFrom 'add_cart_item', request.POST: \n", request.POST)
        
        # _______________________get product variations._______________________
        if request.method == 'POST':
            for key, value in request.POST.items():
                try:
                    variation = Variation.objects.get(product=product, variation_category__iexact=key, variation_value__iexact=value)
                    product_variation.append(variation)
                except:
                    pass
        # ________________________________Done_________________________________

        # ________________________________get cart_items.________________________________
        # Chek if cart_item exists
        if CartItem.objects.filter(product=product, user=current_user).exists():
            cart_items = CartItem.objects.filter(product=product, user=current_user)
            ex_var_list = []
            id_list = []
            for item in cart_items:
                existing_variation = item.variations.all()
                ex_var_list.append(list(existing_variation))
                id_list.append(item.id)

            if product_variation in ex_var_list:
                index = ex_var_list.index(product_variation)
                item_id = id_list[index]
                cart_item = CartItem.objects.get(id=item_id)
                if cart_item.quantity < cart_item.product.stock:
                    cart_item.quantity += 1
                else:
                    cart_item.quantity = cart_item.product.stock 
                cart_item.save()

            else:
                cart_item = CartItem.objects.create(product=product, quantity=1, user=current_user)
                if len(product_variation) > 0:
                    cart_item.variations.add(*product_variation)
                cart_item.save()

        else:
            cart_item = CartItem.objects.create(product=product, user=current_user, quantity=1)
            if len(product_variation) > 0:
                # cart_item.variations.clear()
                cart_item.variations.add(*product_variation)
            cart_item.save()
        # ______________________________________Done______________________________________

        # cart_item.save()
        return redirect('cart')

    # If the user is not authenticated
    else:
        product_variation = []
        print("\n\n\n\nFrom 'add_cart_item', request.POST: \n", request.POST)
        
        # _______________________get product variations._______________________
        if request.method == 'POST':
            for key, value in request.POST.items():
                try:
                    variation = Variation.objects.get(product=product, variation_category__iexact=key, variation_value__iexact=value)
                    product_variation.append(variation)
                except:
                    pass
        # ________________________________Done_________________________________


        # ______________________________get cart._______________________________
        try:
            print("\n--------------------------Call '_cart_id()' in 'carts.views.add_cart_item'.--------------------------")
            cart = Cart.objects.get(cart_id=_cart_id(request))
        except Cart.DoesNotExist:
            print("Just created a new cart by calling '_cart_id()' in 'carts.views.add_cart_item'.")
            cart = Cart.objects.create(cart_id=_cart_id(request))
            cart.save()
        # _________________________________Done_________________________________


        # ________________________________get cart_items.________________________________

        # Chek if cart_item exists
        if CartItem.objects.filter(product=product, cart=cart).exists():
            cart_items = CartItem.objects.filter(product=product, cart=cart)
            ex_var_list = []
            id_list = []
            for item in cart_items:
                existing_variation = item.variations.all()
                ex_var_list.append(list(existing_variation))
                id_list.append(item.id)

            if product_variation in ex_var_list:
                index = ex_var_list.index(product_variation)
                item_id = id_list[index]
                cart_item = CartItem.objects.get(id=item_id)
                if cart_item.quantity < cart_item.product.stock:
                    cart_item.quantity += 1
                else:
                    cart_item.quantity = cart_item.product.stock 
                cart_item.save()

            else:
                cart_item = CartItem.objects.create(product=product, quantity=1, cart=cart)
                if len(product_variation) > 0:
                    cart_item.variations.add(*product_variation)
                cart_item.save()

        else:
            cart_item = CartItem.objects.create(product=product, cart=cart, quantity=1)
            if len(product_variation) > 0:
                # cart_item.variations.clear()
                cart_item.variations.add(*product_variation)
            cart_item.save()
        # ______________________________________Done______________________________________

        # cart_item.save()
        return redirect('cart')




def subtract_cart_item(request, product_id, cart_item_id):
    print("\n--------------------------Call '_cart_id()' in 'carts.views.subtract_cart_item'.--------------------------")
    product = get_object_or_404(Product, id = product_id)
    try:
        if request.user.is_authenticated:
            cart_item = CartItem.objects.get(product=product, user=request.user, id=cart_item_id)
        else:
            cart = Cart.objects.get(cart_id = _cart_id(request))
            
            cart_item = CartItem.objects.get(cart=cart, product=product, id=cart_item_id)
        if cart_item.quantity > 1:
            cart_item.quantity -= 1
            cart_item.save()
        else:
            cart_item.delete()
    except:
        raise Exception("Check the view 'subtract_cart_item'.")
    return redirect('cart')


# or we can just simply do this. Doing this way required deleting '<int:product_id>/'in urls.py and 'cart_item.product.id' in minus button in cart.html, only send 'cart_item.id'

# def subtract_cart_item(request, cart_item_id):
#     cart_item = CartItem.objects.get(id=cart_item_id)
#     if cart_item.quantity > 1:
#         cart_item.quantity -= 1
#         cart_item.save()
#     else:
#         cart_item.delete()
#     return redirect('cart')




def remove_cart_item(request, product_id, cart_item_id):
    product = get_object_or_404(Product, id = product_id)
    if request.user.is_authenticated:
        cart_item = CartItem.objects.get(product=product, user=request.user, id=cart_item_id)
    else:
        cart = Cart.objects.get(cart_id = _cart_id(request))
        cart_item = CartItem.objects.get(cart=cart, product=product, id=cart_item_id)
    
    cart_item.delete()
    return redirect('cart')


# or we can simply do this

# def remove_cart_item(request, product_id, cart_item_id):
#     cart_item = CartItem.objects.get(id=cart_item_id)
#     cart_item.delete()
#     return redirect('cart')




def cart(request, total=0, cart_items=0, quantity=0, discount=0):
    tax = 0
    grand_total= 0
    applied = False

    if request.method == 'POST':
        # Discount
        coupon_code = request.POST['coupon_code']
        coupon = Coupon.objects.get(coupon_code=coupon_code)
        coupon_users = coupon.users.all()

        print(f"\n\n\n\n\n\n\n\ncoupon_users: {coupon_users}")
        print(f"request.user.email: {request.user.email}")

        for i in coupon_users:
            if request.user.email == i.email:
                print(f"i.email: {i.email}\n\n\n\n\n\n\n\n")

                discount = coupon.sale_off_percent
                messages.success(request, "Coupon applied successfully.")
                applied = True

        if applied == False:
            messages.error(request, "You haven't get this coupon.")


    try:
        print("\n--------------------------Call '_cart_id()' in 'carts.views.cart'.--------------------------")
        
        if request.user.is_authenticated:
            cart_items = CartItem.objects.filter(user=request.user, is_active=True)
        else:
            cart = Cart.objects.get(cart_id = _cart_id(request))
            cart_items = CartItem.objects.filter(cart=cart, is_active=True)


        for cart_item in cart_items:
            total += cart_item.quantity * cart_item.product.price
            quantity += 1
        
        discounted_total = total * ( 1 - discount/100 )

        tax = total * 0.02
        grand_total = discounted_total + tax
    except ObjectDoesNotExist:
        pass    # Do nothing. No cart is created here. Creating a cart ('Cart' instance) is executed only when users 
                # click 'Add to cart', which call 'add_cart_item' to execute the cart creation (If there hasn't been one yet).
                #  
    
    context = {
            'cart_items': cart_items,
            'quantity': quantity,
            'total': total,
            'tax': tax,
            'grand_total': grand_total,
            'discounted_total': discounted_total,
            'applied': applied
        }
    return render(request, 'store/cart.html', context)



@login_required(login_url = 'login')
def checkout(request, total=0, cart_items=0, quantity=0):
    discounted_total = request.POST['discounted_total']

    tax = 0
    grand_total= 0
    try:
        print("\n--------------------------Call '_cart_id()' in 'carts.views.cart'.--------------------------")
        if request.user.is_authenticated:
            cart_items = CartItem.objects.filter(user=request.user, is_active=True)
        else:
            cart = Cart.objects.get(cart_id = _cart_id(request))
            cart_items = CartItem.objects.filter(cart=cart, is_active=True)
        
        for cart_item in cart_items:
            total += cart_item.quantity * cart_item.product.price
            quantity += 1
        
        tax = total * 0.02
        grand_total = total + tax
    except ObjectDoesNotExist:
        pass    # Do nothing. No cart is created here. Creating a cart ('Cart' instance) is executed only when users 
                # click 'Add to cart', which call 'add_cart_item' to execute the cart creation (If there hasn't been one yet).
    context = {
            'cart_items': cart_items,
            'quantity': quantity,
            'total': total,
            'tax': tax,
            'grand_total': grand_total,
            'discounted_total': discounted_total
        }
    return render(request, 'store/checkout.html', context)