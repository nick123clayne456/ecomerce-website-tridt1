# Generated by Django 4.0.4 on 2022-06-21 10:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0008_remove_coupon_product_coupon_coupon_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='coupon',
            old_name='coupon',
            new_name='products',
        ),
        migrations.RenameField(
            model_name='coupon',
            old_name='user',
            new_name='users',
        ),
    ]
