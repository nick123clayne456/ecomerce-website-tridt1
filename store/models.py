from urllib.parse import MAX_CACHE_SIZE
from django.db import models

from category.models import Category
from django.urls import reverse

from accounts.models import Account

from django.db.models import Avg, Count

# Create your models here.

class Product(models.Model):
    product_name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, unique=True)
    description = models.TextField(max_length=500)
    price = models.IntegerField()
    images = models.ImageField(upload_to='photos/products')
    stock = models.IntegerField()
    is_available = models.BooleanField(default=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def get_url(self):
        return reverse('product_detail', args=[self.category.slug, self.slug])

    def __str__(self):
        return self.product_name

    def averageRating(self):
        ratings = ReviewRating.objects.filter(product=self, status=True).aggregate(average=Avg('rating'))
        avg_rt = 0
        if ratings['average'] is not None:
            avg_rt = float(ratings['average'])
        return avg_rt

    def countReview(self):
        reviews = ReviewRating.objects.filter(product=self, status=True).aggregate(count=Count('review'))
        rv_count = 0
        if reviews['count'] is not None:
            rv_count = int(reviews['count'])
        return rv_count

        # or we can do this
        
        # reviews = ReviewRating.objects.filter(product=self, review__regex=r'[^\s]')
        # return reviews.count()




class VariationManager(models.Manager):
    def colors(self):
        return super(VariationManager, self).filter(variation_category='color', is_active=True)

    def sizes(self):
        return super(VariationManager, self).filter(variation_category='size', is_active=True)


variation_category_choice = (
    ('color', 'Color'),
    ('size', 'Size')
)

class Variation(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    variation_category = models.CharField(max_length=100, choices=variation_category_choice)
    variation_value = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now=True)

    objects = VariationManager()

    def __str__(self):
        return self.variation_value

    
class ReviewRating(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    subject = models.CharField(max_length=100, blank=True)
    review = models.TextField(max_length=500, blank=True)
    rating = models.FloatField()
    ip = models.CharField(max_length=20, blank=True)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.subject





class ProductGallery(models.Model):
    product = models.ForeignKey(Product, default=None, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='store/products')

    def __str__(self):
        return self.product.product_name

    class Meta:
        verbose_name = 'productgallery'
        verbose_name_plural = 'product gallery'






class Coupon(models.Model):
    coupon_code = models.CharField(max_length=200)
    users = models.ManyToManyField(Account, blank=True)
    products = models.ManyToManyField(Product, blank=True)
    sale_off_percent = models.IntegerField(null=True, blank=True)
    stock = models.IntegerField()

    def __str__(self):
        return self.coupon_code


