from functools import total_ordering
from io import BytesIO
from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse

from carts.models import Cart, CartItem

from .forms import OrderForm
from .models import Order, OrderProduct

from datetime import datetime, date

from django.contrib import messages

from orders.models import Payment

import json

from store.models import Product

from django.core.mail import EmailMessage
from django.template.loader import render_to_string

import os
from xhtml2pdf import pisa

from django.utils.html import strip_tags

from django.core import mail

# Create your views here.


def place_order(request, total = 0, quantity=0):
    current_user = request.user

    discounted_total = request.POST['discounted_total']    

    cart_items = CartItem.objects.filter(user=current_user)
    cart_count = cart_items.count()
    if cart_count <= 0:
        return redirect('store')

    grand_total = 0
    tax = 0
    for cart_item in cart_items:
        # total += cart_item.product.price * cart_item.quantity
        total += cart_item.sub_total()
        quantity += cart_item.quantity

    tax = 0.02 * total
    grand_total = float(discounted_total) + tax


    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            # Store all the billing information inside Order table
            order = Order()
            order.user = current_user
            order.first_name = form.cleaned_data['first_name']
            order.last_name = form.cleaned_data['last_name']
            order.phone = form.cleaned_data['phone']
            order.email = form.cleaned_data['email']
            order.address_line_1 = form.cleaned_data['address_line_1']
            order.address_line_2 = form.cleaned_data['address_line_2']
            order.country = form.cleaned_data['country']
            order.district = form.cleaned_data['district']
            order.city = form.cleaned_data['city']
            order.order_note = form.cleaned_data['order_note']

            order.order_total = grand_total
            order.tax = tax
            order.ip = request.META.get('REMOTE_ADDR')
            order.save()

            yr = int(date.today().strftime('%Y'))
            dt = int(date.today().strftime('%d'))
            mt = int(date.today().strftime('%m'))
            d = date(yr, mt, dt)
            current_date = d.strftime('%Y%d%m')
            order.order_number = current_date + str(order.id)
            order.save()

            context = {
                'order': order,
                'cart_items': cart_items,
                'total': total,
                'tax': tax,
                'grand_total': grand_total,
                'discounted_total': discounted_total
            }

            # messages.success(request, 'Placing order successfully.')
            return render(request, 'orders/payments.html', context)
        else:
            print("\n\n\n\n Invalid form received in 'place_order'.\n\n\n\n")
            return redirect('checkout')




def payments(request):
    body = json.loads(request.body)
    order = Order.objects.get(user=request.user, is_ordered=False, order_number=body['orderID'])

    # Store transaction details inside Payment model
    payment = Payment(
        user = request.user,
        payment_id = body['transID'],
        payment_method = body['payment_method'],
        amount_paid = order.order_total,
        status = body['status'],
    )
    payment.save()

    order.payment = payment
    order.is_ordered = True
    order.save()


    # Move the cart item to OrderProduct table
    cart_items = CartItem.objects.filter(user=request.user)
    for item in cart_items:
        orderproduct = OrderProduct()
        orderproduct.order_id = order.id
        orderproduct.payment = payment
        orderproduct.user_id = request.user.id
        orderproduct.product_id = item.product_id
        orderproduct.quantity = item.quantity
        orderproduct.product_price = item.product.price
        orderproduct.ordered = True
        orderproduct.save()

        cart_item = CartItem.objects.get(id=item.id)
        product_variation = cart_item.variations.all()
        orderproduct = OrderProduct.objects.get(id=orderproduct.id)
        orderproduct.variations.set(product_variation)
        orderproduct.save()


        # Reduce the quantity of sold products
        product = Product.objects.get(id=item.product_id)
        product.stock -= item.quantity
        product.save()

    
    # Clear cart
    CartItem.objects.filter(user=request.user).delete()


    # Send order received email
    ordered_products = OrderProduct.objects.filter(order_id=order.id)

    subtotal= 0
    for i in ordered_products:
        subtotal += i.sub_total()

    context = {
        'user': request.user,
        'order': order,
        'ordered_products': ordered_products,
        'subtotal': subtotal
    }


    # Save pdf file
    rendered_template = render_to_string('reports/pdf_report.html', context)

    if f'{order.order_number}.pdf' not in os.listdir('PDF_Reports'):    # os.listdir('PDF_Reports') return a list containing the names of files in 'PDF_Reports' directory.
        pdf_file = open(f'.\PDF_reports\{order.order_number}.pdf', "w+b")

        pisa.CreatePDF(rendered_template, dest=pdf_file)    # Save the pdf file in 'PDF_Reports' directory.
        pdf_file.close()

    mail_subject = 'Thank you for ordering.'
    message = render_to_string('orders/orders_received_email.html', context)
    plain_message = strip_tags(message)
    to_email = request.user.email
    send_email = EmailMessage(mail_subject, message, to=[to_email,])
    send_email.content_subtype = 'html'
    send_email.attach_file(f"PDF_Reports\{order.order_number}.pdf")
    send_email.send()

    # mail.send_mail(mail_subject, plain_message, 'atom1try@gmail.com', [to_email], html_message=message)


    # Send order number and transaction id back to sendData method in 'payments.html' script via JsonResponse
    data = {
        'order_number': order.order_number,
        'transID': payment.payment_id,
    }

    return JsonResponse(data)





def order_complete(request):
    order_number = request.GET.get('order_number')
    transID = request.GET.get('payment_id')

    try:
        order = Order.objects.get(order_number=order_number, is_ordered=True)
        ordered_products = OrderProduct.objects.filter(order_id=order.id)

        subtotal= 0
        for i in ordered_products:
            subtotal += i.product_price * i.quantity

        payment = Payment.objects.get(payment_id=transID)

        context = {
            'order': order,
            'ordered_products': ordered_products,
            'transID': transID,
            'payment': payment,
            'subtotal': subtotal
        }
        return render(request, 'orders/order_complete.html', context)
    except (Payment.DoesNotExist, Order.DoesNotExist):
        return redirect('home')






def pdf_report(request):
    order_id = request.GET['order_id']
    order = Order.objects.get(order_number=order_id, is_ordered=True)
    ordered_products = OrderProduct.objects.filter(order_id=order.id)

    subtotal= 0
    for i in ordered_products:
        subtotal += i.sub_total()

    context = {
        'order': order,
        'ordered_products': ordered_products,
        'subtotal': subtotal
    }

    rendered_template = render_to_string('reports/pdf_report.html', context)    # The html file witf class SafeString.


    # Show PDF file
    buffer = BytesIO()  # Create a buffer to contain the binary data of the rendered template.
    pdf = pisa.pisaDocument(BytesIO(rendered_template.encode('utf-8')), buffer) # Write the rendered template into the buffer

    if not pdf.err:
        return HttpResponse(buffer.getvalue(), content_type='application/pdf')
    return None
