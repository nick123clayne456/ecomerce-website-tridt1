$(document).ready(function(){    
    $("#loadMore").on('click',function(){
        var _currentReviews = $(".review-box").length;
        var _limit = $(this).attr('data-limit');
        var _total = $(this).attr('data-total');
        var _currentProduct=$(this).attr('this_prod_id');

        console.log(_currentReviews, _limit, _total);
        // Start Ajax
        $.ajax({
            url:'/store/load_more_review',
            data:{
                limit:_limit,
                offset:_currentReviews,
                current_product: _currentProduct
            },
            dataType: 'json',
            beforeSend:function(){
                $("#loadMore").attr('disabled', true);
                $(".load-more-icon").addClass('fa-spin');
            },
            success:function(res){
                $("#loadReviewHere").append(res.data);
                $("#loadMore").attr('disabled', false);
                $(".load-more-icon").removeClass('fa-spin');

                var _totalShowing=$(".review-box").length;
                if(_totalShowing == _total){
                    $("#loadMore").remove();
                }
            }
        });
        // End Ajax
    });
});