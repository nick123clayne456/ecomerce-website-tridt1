from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from .forms import RegistrationForm, UserForm, UserProfileForm
from .models import Account, UserProfile

from django.shortcuts import redirect

from django.contrib import messages, auth

from django.contrib.auth.decorators import login_required

# Verification
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMessage

from carts.models import Cart, CartItem
from carts.views import _cart_id
import requests

from orders.models import Order, OrderProduct

# Create your views here.

def login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        user = auth.authenticate(email=email, password=password)

        if user is not None:
            try:
                cart = Cart.objects.get(cart_id=_cart_id(request))

                if CartItem.objects.filter(cart=cart).exists(): 
                    # Get cart items from current anonymous cart
                    cart_items_anonymous = CartItem.objects.filter(cart=cart)
                    anonymous_cart_item_variation_list = []
                    anonymous_cart_item_id_list = []
                    for cart_item in cart_items_anonymous:
                        anonymous_cart_item_variation_list.append(list(cart_item.variations.all()))
                        anonymous_cart_item_id_list.append(cart_item.id)

                    # Get cart items from this user's previous cart.
                    cart_items_user = CartItem.objects.filter(user=user)
                    user_cart_item_variation_list = []
                    user_cart_item_id_list = []
                    for cart_item in cart_items_user:
                        user_cart_item_variation_list.append(list(cart_item.variations.all()))
                        user_cart_item_id_list.append(cart_item.id)

                    for single_cart_item_variation_list in anonymous_cart_item_variation_list:
                        if single_cart_item_variation_list in user_cart_item_variation_list:
                            the_user_cart_item_id = user_cart_item_id_list[user_cart_item_variation_list.index(single_cart_item_variation_list)]
                            cart_item = CartItem.objects.get(id = the_user_cart_item_id)
                            cart_item.quantity += 1
                            cart_item.save()
                        else:
                            the_anonymous_cart_item_id = anonymous_cart_item_id_list[anonymous_cart_item_variation_list.index(single_cart_item_variation_list)]
                            new_cart_item_to_add = CartItem.objects.get(id = the_anonymous_cart_item_id)
                            new_cart_item_to_add.user = user
                            new_cart_item_to_add.save()

            except:
                pass
            auth.login(request, user)
            messages.success(request, 'You are logged in.')

            url = request.META.get('HTTP_REFERER')  # Get the previous url.
            try:
                query = requests.utils.urlparse(url).query
                params = dict(x.split('=') for x in query.split('@'))
                if 'next' in params:
                    nextPage = params['next']
                    return redirect(nextPage)
            except:
                return redirect('dashboard')

        else:
            messages.error(request, "Invalid email or password.")
            return redirect('login')
    
    return render(request, 'accounts/login.html')




@login_required(login_url = 'login')
def logout(request):
    auth.logout(request)
    messages.success(request, 'You are logged our.')
    return redirect('login')




def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            phone_number = form.cleaned_data['phone_number']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            # Automatically create username.
            username = email.split('@')[0]

            user = Account.objects.create_user(first_name=first_name, last_name=last_name, email=email, password=password, username=username)
            user.phone_number = phone_number
            user.save()

            # Automatically create a user profu=ile according to the registering user.
            profile = UserProfile()
            profile.user_id = user.id
            profile.profile_picture = '/default/default-user-avatar.png'
            profile.save()


            current_site = get_current_site(request)
            mail_subject = 'Please activate your account.'
            message = render_to_string('accounts/account_verification_email.html', {
                'user': user,
                'domain': current_site,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user)
            })
            to_email = email
            send_email = EmailMessage(mail_subject, message, to=[to_email])
            send_email.send()

            # messages.success(request, "We've sent you a verification email. Please check.")
            return redirect('/accounts/login/?command=verification&email='+email)
    else:
        form = RegistrationForm()
    context = {
        'form': form,
    }
    return render(request, 'accounts/register.html', context)




def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = Account._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Account.DoesNotExist):
        user = None
    
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(request, 'Your account is activated.')
        return redirect('login')
    else:
        messages.error(request, 'Invalid activation link.')
        return redirect('register')




@login_required(login_url = 'login')
def dashboard(request):
    orders = Order.objects.order_by('-created_at').filter(user__id=request.user.id, is_ordered=True)
    orders_count = orders.count()

    user_profile = UserProfile.objects.get(user_id=request.user.id)

    context = {
        'orders_count': orders_count,
        'user_profile': user_profile
    }
    return render(request, 'accounts/dashboard.html', context)



def forgotPassword(request):
    if request.method == 'POST':
        email = request.POST['email']
        if Account.objects.filter(email=email).exists():
            user = Account.objects.get(email__exact=email)

            current_site = get_current_site(request)
            mail_subject = 'Reset your password.'
            message = render_to_string('accounts/reset_password_email.html', {
                'user': user,
                'domain': current_site,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user)
            })
            to_email = email
            send_email = EmailMessage(mail_subject, message, to=[to_email])
            send_email.send()
            messages.success(request, 'Password reset email has been sent to your email address.')
            return redirect('login')

        else:
            messages.error(request, 'Account does not exist.')
            return redirect('forgotPassword')
    return render(request, 'accounts/forgotPassword.html')



def resetpassword_validate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = Account.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Account.DoesNotExist):
        user = None
    
    if user is not None and default_token_generator.check_token(user, token):
        request.session['uid'] = uid
        messages.success(request, 'Reset your password')
        return redirect('resetPassword')
    else:
        messages.error(request, 'This link expired.')
        return redirect('login')



def resetPassword(request):
    if request.method == 'POST':
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        if password == confirm_password:
            uid = request.session.get('uid')
            user = Account.objects.get(pk=uid)
            user.set_password(password)
            user.save()
            messages.success(request, 'Password reset successfully.')
            return redirect('login')
        else:
            messages.error(request, "Password do not match.")
            return redirect('resetPassword')
    else:
        return render(request, 'accounts/resetPassword.html')




@login_required(login_url='login')
def my_orders(request):
    orders = Order.objects.filter(user=request.user, is_ordered=True)

    context = {
        'orders': orders
    }
    return render(request, 'accounts/my_orders.html', context)




@login_required(login_url='login')
def edit_profile(request):
    userprofile = get_object_or_404(UserProfile, user=request.user)

    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = UserProfileForm(request.POST, request.FILES, instance=userprofile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Your profile has been updated.')
            return redirect('edit_profile')
    else:
        user_form = UserForm(instance=request.user)
        profile_form = UserProfileForm(instance = userprofile)

    context = {
        'user_form': user_form,
        'profile_form': profile_form,
        'userprofile': userprofile
    }

    return render(request, 'accounts/edit_profile.html', context)





@login_required(login_url='login')  # @login_required actually force loggin out when we refresh the browser's url, so no need for 'auth.logout(request)'.
def change_password(request):
    if request.method == 'POST':
        current_password = request.POST['current_password']
        new_password = request.POST['new_password']
        confirm_password = request.POST['confirm_password']

        user = Account.objects.get(email__exact=request.user.email)

        if new_password == confirm_password:
            if user.check_password(current_password):
                user.set_password(new_password)
                user.save()

                messages.success(request, 'Password updated successfully.')
            else:
                messages.error(request, "Invalid current password.")
                
            return redirect('change_password')

        else:
            messages.error(request, "Confirm password does not match.")
            return redirect('change_password')

    return render(request, 'accounts/change_password.html')




@login_required(login_url = 'login')
def order_detail(request, order_id):
    order_products = OrderProduct.objects.filter(order__order_number=order_id)
    order = Order.objects.get(order_number=order_id)

    subtotal = 0
    for i in order_products:
        subtotal += i.sub_total()
    

    context = {
        'order_products': order_products,
        'order': order,
        'subtotal': subtotal
    }
    return render(request, 'accounts/order_detail.html', context)